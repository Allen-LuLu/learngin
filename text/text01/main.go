package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	r.LoadHTMLFiles("./static/index.html")
	r.Static("/static", "./static")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	r.GET("/api/ajax", func(c *gin.Context) {
		userName := c.Query("username")
		fmt.Println(userName)
		if userName == "admin" {
			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status": "err",
			})
		}
	})

	r.Run(":9090")
}
