package main

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// 初始化数据库
func initDB(username, password, hosts, databaseName, dataBase string) (*gorm.DB, error) {
	dsn := fmt.Sprintf("%s:%s@(%s)/%s", username, password, hosts, databaseName)
	return gorm.Open(dataBase, dsn)
}

/* CRUD 接口*/

/*gorm包内置了一个gorm.Model类型结构体，可以帮我们创建id主键，以及维护创建时间，更新时间，删除时间
type Model struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}
*/

//type UserInfo struct {
//	gorm.Model
//	Name         string
//	Age          sql.NullInt64 // 设置0值类型
//	Birthday     *time.Time
//	Email        string `gorm:"type:varchar(100);unique_index"` // email字段作为唯一索引
//	Role         string `gorm:"size:255"`                       // 设置字段大小,string类型会默认映射为varchar
//	MemberNumber string `gorm:"unique;not null"`
//	Num          int    `gorm:"AUTO_INCREMENT"`
//	Address      string `gorm:"index:addr"` // 为address字段创建一个名为addr的索引
//	IgnoreMe     int    `gorm:"-"`          // 使用"-"将不会映射该字段到表
//}

type User struct {
	ID   int
	Name string `gorm:"default:'小王子'"`
	Age  int
}

func newUser(name string, age int) *User {
	return &User{
		Name: name,
		Age:  age,
	}
}

//func (User) TableName() string {  // 也可以通过给结构体指定一个方法来修改默认表名
//	return "user"
//}

func createTable(db *gorm.DB, tableName string) {
	//db.AutoMigrate(&User{}) //它只会添加新的字段，而不会删除原表原数据
	db = db.Table("user").DropTableIfExists(&User{})
	db.Table("user").CreateTable(&User{})
}

// 增
func insert(db *gorm.DB, name string, age int) error {
	u := newUser(name, age)
	ok := db.NewRecord(u) // 根据主键判断该记录是否为新纪录，如果表中已有这条记录，则返回false
	if !ok {
		return errors.New("the record is exists\n")
	}
	db.Create(u)
	return nil

}

// 删
func delete(db *gorm.DB) {
	// delete删除要保证结构体中的id字段有值,不推荐
	//db.Delete(&User{ID: 2})

	// 批量删除
	db.Where("name=?", "河清").Delete(User{})
}

// 改
func update(db *gorm.DB) {
	// 根据对象更新
	var user User
	db.First(&user)
	db.Model(&user).Update("age", 17)

	// 使用sql表达式更新
	db.Model(&user).Update("age", gorm.Expr("age + ?", 2))

}

// 查
func query(db *gorm.DB) {
	/******************************** 一般查询*****************************/
	// 根据主键查询第一条记录
	//var user User
	//db.First(&user)
	//fmt.Printf("first:%#v\n", user)
	// 随机获取一条记录
	//db.Take(&user)
	//fmt.Printf("take:%#v\n", user)
	// 根据主键查询最后一条记录
	//db.Last(&user)
	//fmt.Printf("last:%#v\n", user)
	////查询所有记录
	var users []User
	//db.Find(&users)
	//for _, u := range users {
	//	fmt.Printf("users:%#v\n", u)
	//}
	//查询指定的某条记录(仅当主键为整型时可用)
	//db.First(&user, 10) // 查询id=10的记录
	//fmt.Printf("%#v\n", user)
	/*************************** where条件查询********************************/
	// 根据sql语句查询
	//db.Where("name like ?", "河%").Find(&users) // 如果找不到，会返回一个user对象，所有字段默认零值
	//for _, u := range users {
	//	fmt.Printf("users:%#v\n", u)
	//}
	// 根据map查询
	//db.Where(map[string]interface{}{"name": "河清", "age": 18}).Find(&users) // 如果找不到，会返回一个user对象，所有字段默认零值
	//for _, u := range users {
	//	fmt.Printf("users:%#v\n", u)
	//}
	// 根据主键的切片查询
	/************************查询指定字段*************************/
	//db.Select("name").Where("id=?", 1).Find(&users)
	//for _, u := range users {
	//	fmt.Printf("users:%#v\n", u)
	//}
	/************************排序查询***************************/
	// 单字段排序
	//db.Order("age desc,name").Find(&users)
	//for _, u := range users {
	//	fmt.Printf("users:%#v\n", u)
	//}
	// 多字段排序
	//db.Order("age desc").Order("name").Find(&users)
	//for _, u := range users {
	//	fmt.Printf("users:%#v\n", u)
	//}
	/***********************指定数量limit***********************/
	db.Limit(1).Where("name=?", "河清").Find(&users)
	for _, u := range users {
		fmt.Printf("users:%#v\n", u)
	}

	/*************************子查询*****************************/
	//db.Where("count>?",db.Table("others").Select("AVG(count)").Where("name=?","lhq").QueryExpr())
}

func main() {
	db, err := initDB("root", "Lyx963421.", "127.0.0.1", "gorm", "mysql")
	if err != nil {
		fmt.Printf("connect db failed,err:%v\n", err)
		return
	}
	// 建立表并映射
	//createTable(db, "user")

	db = db.Table("user") // 改了表明必须指定数据库绑定自己自定义的表
	// 插入
	//err = insert(db, "河清", 18)
	//err = insert(db, "你", 17)
	//if err != nil {
	//	fmt.Printf("insert failed,err:%v\n", err)
	//	return
	//}
	//query(db)
	//update(db)
	delete(db)
	defer db.Close()
}
