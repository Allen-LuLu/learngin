package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// 学习gin框架的路由与路由组

func main() {
	r := gin.Default()

	// 当请求的是没有定义的路由时，可以r.NoRoute()来处理
	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "not found",
		})
	})

	// 路由组
	booksGroup := r.Group("/books") // 通过路由组统一管理一组具有公共前缀的路由
	{
		idGroup := booksGroup.Group("/id") // 可以进行路由组嵌套
		{
			idGroup.GET("/:nums", func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{
					"nums": c.Param("nums"),
				})
			})
		}
		booksGroup.GET("/index", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "booksIndex",
			})
		})
	}

	r.Run(":9090")
}
