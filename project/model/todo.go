package model

type Todo struct {
	Id     uint   `json:"id"`
	Title  string `json:"title"`
	Status bool   `json:"status"`
}
