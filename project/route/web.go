package route

import (
	"gin/project/controller"
	"github.com/gin-gonic/gin"
)

//TodoController 是一个全局的控制器，用于控制model todo的相关路由
var TodoController *controller.ToDoController

func ConfigRoute() (*gin.Engine, error) {
	r := gin.Default()
	var err error
	TodoController, err = controller.NewToDoController() // 初始化一个controller
	if err != nil {
		return nil, err
	}
	r.Static("/static", "./static") // 注册静态资源

	r.LoadHTMLGlob("./templates/*") // 加载模板

	r.GET("/", controller.Index) // 配置根目录路由

	// 配置v1路由组
	v1Group := r.Group("/v1")
	{
		// 添加事件
		v1Group.POST("/todo", TodoController.Add)
		// 查询所有事件
		v1Group.GET("/todo", TodoController.Find)
		// 更新事件
		v1Group.PUT("/todo/:id", TodoController.Set)
		// 删除事件
		v1Group.DELETE("/todo/:id", TodoController.Delete)
	}

	return r, nil
}
