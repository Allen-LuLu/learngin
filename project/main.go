package main

import (
	"fmt"
	"gin/project/route"
)

func main() {

	// 进行路由配置
	r, err := route.ConfigRoute()
	// 路由配置不成功，项目都没有启动的必要，直接panic
	if err != nil {
		panic(err)
	}

	// 监听端口
	err = r.Run(":9090")
	if err != nil {
		fmt.Printf("listen failed,err:%v\n", err)
		return
	}

	// 调用接口关闭数据库
	defer route.TodoController.Close()
}
