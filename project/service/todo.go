package service

import (
	"gin/project/model"
	"github.com/jinzhu/gorm"
)

//Todo 模型todo对应的接口
type Todo interface {
	//AddToDo 添加事件
	AddToDo(todo *model.Todo) error
	//FindAll 查询所有事务
	FindAll() ([]model.Todo, error)
	//SetStatusById 通过ID设置状态
	SetStatusById(id int) error
	//DeleteById 通过ID删除事件
	DeleteById(id int) error
	//Close 关闭数据库
	Close()
}

type todo struct {
	Db *gorm.DB
}

//NewToDO 实例化一个接口对象
func NewToDO(db *gorm.DB) Todo {
	return &todo{
		Db: db,
	}
}

func (td *todo) AddToDo(todo *model.Todo) error {
	return td.Db.Create(todo).Error
}

func (td *todo) SetStatusById(id int) error {
	var todo model.Todo
	err := td.Db.Where("id=?", id).First(&todo).Error
	if err != nil {
		return err
	}
	err = td.Db.Model(&todo).Update("status", !todo.Status).Error
	if err != nil {
		return err
	}
	return nil
}

func (td *todo) FindAll() ([]model.Todo, error) {
	var todoList []model.Todo
	err := td.Db.Find(&todoList).Error
	if err != nil {
		return nil, err
	}
	return todoList, nil
}

func (td *todo) DeleteById(id int) error {
	return td.Db.Where("id=?", id).Delete(&model.Todo{}).Error
}

//Close service提供接口关闭数据库
func (td *todo) Close() {
	td.Db.Close()
}
