package controller

import (
	"gin/project/dao"
	"gin/project/model"
	"gin/project/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

//ToDoController 用于控制todo模型的相关操作
type ToDoController struct {
	service service.Todo // 每个controller绑定一个对应的service,service负责处理数据库的业务逻辑
}

//NewToDoController 初始化一个ToDoController
func NewToDoController() (*ToDoController, error) {
	db, err := dao.InitDB() //由于service负责数据库操作，因此service需要绑定一个数据库实例
	if err != nil {
		return nil, err
	}
	todo := service.NewToDO(db) //初始化一个service
	return &ToDoController{
		service: todo,
	}, nil
}

//Index 起始页面
func Index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}

//Add 添加事物的路由处理函数
func (c *ToDoController) Add(ctx *gin.Context) {
	var todo model.Todo
	err := ctx.BindJSON(&todo) //获取前端传来的json数据
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
		return
	}
	err = c.service.AddToDo(&todo) // 在数据库创建记录
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, todo) // 返回记录给前端

}

//Find 添加事物的路由处理函数
func (c *ToDoController) Find(ctx *gin.Context) {
	todoList, err := c.service.FindAll() // 从数据库中查询所有记录
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
	}
	ctx.JSON(http.StatusOK, todoList) // 将记录返回给前端
}

//Set 更新事件的状态
func (c *ToDoController) Set(ctx *gin.Context) {

	idStr := ctx.Param("id")
	id, _ := strconv.Atoi(idStr)
	err := c.service.SetStatusById(id) // service修改数据库中id对应的记录的status值
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"Error": err.Error(),
		})
	}
}

//Delete 删除记录
func (c *ToDoController) Delete(ctx *gin.Context) {
	idStr := ctx.Param("id")
	id, _ := strconv.Atoi(idStr)
	err := c.service.DeleteById(id) // service删除数据库中id对应的记录
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status": "success",
	})
}

//Close 提供main函数关闭数据库的接口
func (c *ToDoController) Close() {
	c.service.Close()
}
