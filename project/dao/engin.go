package dao

import (
	"gin/project/model"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

//InitDB 连接数据库并返回一个数据库实例
func InitDB() (db *gorm.DB, err error) {
	dsn := "root:Lyx963421.@tcp(127.0.0.1:3306)/bubble"
	db, err = gorm.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	err = db.AutoMigrate(&model.Todo{}).Error
	if err != nil {
		return nil, err
	}
	return
}
