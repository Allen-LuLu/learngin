package main

import (
	"fmt"
	"net/http"
	"text/template"
)

func nesting(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("./t.tmpl", "./ul.tmpl") //父模板必须在子模板之前
	if err != nil {
		fmt.Printf("parse template failed,err:%v\n", err)
		return
	}
	name := "小王子"
	t.Execute(w, name)
}

func main() {
	http.HandleFunc("/nesting", nesting)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("listen failed,err:%v\n", err)
		return
	}
}
