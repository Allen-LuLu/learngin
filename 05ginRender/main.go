package main

import (
	"github.com/gin-gonic/gin"
	template2 "html/template"
	"net/http"
)

func main() {
	r := gin.Default()
	// 可以加在静态文件，进行映射
	r.Static("/static", "./static")

	// 如果要自定义函数，需在模板解析前定义
	r.SetFuncMap(template2.FuncMap{
		"safe": func(str string) template2.HTML {
			return template2.HTML(str)
		},
	})

	// 解析模板
	r.LoadHTMLGlob("templates/**/*.tmpl") // 通过正则表达式批量解析模板，两个**代表所有目录

	r.GET("/posts/index", func(c *gin.Context) {
		title := "小王子"
		c.HTML(http.StatusOK, "posts/index.tmpl", gin.H{
			"title": title,
		})
	})

	r.GET("/users/index", func(c *gin.Context) {
		title := "<a href='https://www.baidu.com'>百度</a>"
		c.HTML(http.StatusOK, "users/index.tmpl", gin.H{
			"title": title,
		})
	})
	r.Run(":9090")
}
