package main

import (
	"fmt"
	"net/http"
	"text/template"
)

type user struct {
	Name   string
	Gender string //必须大写，否则模板渲染不了
	Age    int
}

// 传入普通变量
func sayHello(w http.ResponseWriter, r *http.Request) {
	// 解析模板
	t, err := template.ParseFiles("./hello.tmpl")
	if err != nil {
		fmt.Printf("template parse failed,err:%v\n", err)
		return
	}

	//渲染模板
	err = t.Execute(w, "卢河清")
	if err != nil {
		fmt.Printf("render template failed,err:%v\n", err)
		return
	}
}

// 传入一个结构体对象
func sayHelloStruct(w http.ResponseWriter, r *http.Request) {
	// 解析模板
	t, err := template.ParseFiles("./hello.tmpl")
	if err != nil {
		fmt.Printf("template parse failed,err:%v\n", err)
		return
	}

	u := user{
		Name:   "lhq",
		Gender: "男",
		Age:    18,
	}

	// 如果想传入多个值，可以通过map操作
	m1 := map[string]interface{}{
		"name":   "lulu",
		"gender": "男",
		"age":    23, // 这里不需要大写
	}

	// 如果传入切片数据，模板还可以使用range循环
	hobbyList := []string{
		"唱",
		"跳",
		"rap",
		"篮球",
	}

	//渲染模板
	err = t.Execute(w, map[string]interface{}{
		"u1":    u,
		"m1":    m1,
		"hobby": hobbyList,
	})
	if err != nil {
		fmt.Printf("render template failed,err:%v\n", err)
		return
	}
}

// 自定义模板函数
func templateFunc(w http.ResponseWriter, r *http.Request) {

	// 自定义一个函数,若该函数要求两个返回值，则最后一个必须是error
	kua := func(name string) (string, error) {
		return name + "真帅", nil
	}

	// 解析模板
	t := template.New("hello.tmpl")
	t.Funcs(template.FuncMap{
		"kua": kua,
	})
	_, err := t.ParseFiles("./hello.tmpl")
	if err != nil {
		fmt.Printf("template parse failed,err:%v\n", err)
		return
	}

	u := user{
		Name:   "lhq",
		Gender: "男",
		Age:    18,
	}

	// 如果想传入多个值，可以通过map操作
	m1 := map[string]interface{}{
		"name":   "lulu",
		"gender": "男",
		"age":    23, // 这里不需要大写
	}

	// 如果传入切片数据，模板还可以使用range循环
	hobbyList := []string{
		"唱",
		"跳",
		"rap",
		"篮球",
	}

	//渲染模板
	err = t.Execute(w, map[string]interface{}{
		"u1":    u,
		"m1":    m1,
		"hobby": hobbyList,
	})
	if err != nil {
		fmt.Printf("render template failed,err:%v\n", err)
		return
	}

}

func main() {
	//http.HandleFunc("/", sayHello)
	//http.HandleFunc("/struct", sayHelloStruct)
	http.HandleFunc("/func", templateFunc)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Printf("listen failed,err:%v\n", err)
		return
	}
}
