package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 1.gin返回json格式数据
func backJson(c *gin.Context) {
	// 第一种方式，gin.H
	//c.JSON(http.StatusOK, gin.H{
	//	"name": "LuLu",
	//	"age":  "18",
	//})

	// 第二种方式,结构体
	var msg = struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}{"lhq", 18}
	c.JSON(http.StatusOK, msg)

}

// 2.gin获取浏览器请求携带的query string参数
func query(c *gin.Context) {
	//name := c.Query("query")              // 查询对应的key
	name := c.DefaultQuery("query", "河清") // 查询对应的key，没有则返回默认值
	c.JSON(http.StatusOK, gin.H{
		"name": name,
	})
}

// 3.gin获取form表单参数

//GetLogin 该函数用于处理从浏览器上输入url点击回车时，浏览器提交的get请求
func GetLogin(c *gin.Context) {
	c.HTML(http.StatusOK, "login.html", nil)
}

//PostLogin 当点击登录按钮时，前端会提交post请求，这时候需要一个post路由处理
func PostLogin(c *gin.Context) {
	username := c.PostForm("username") // key值为前端input标签中的name属性
	password := c.PostForm("password") // 注意，这里只能获取前端传来的form格式数据，如果是json的话这里拿不到

	// 这样才能获取前端的json数据
	//m := make(map[string]interface{})
	//c.BindJSON(&m)

	c.HTML(http.StatusOK, "home.html", gin.H{
		"Name":     username,
		"Password": password,
	})
}

// 4.获取URI参数,eq:http://localhost:9090/user/search/小王子
func uri(c *gin.Context) {
	// 获取路径参数
	username := c.Param("username")
	age := c.Param("age")
	c.JSON(http.StatusOK, gin.H{
		"username": username,
		"age":      age,
	})
}

// 5.gin参数绑定
type user struct {
	Username string `form:"username"`
	Password string `form:"password"`
}

func bind(c *gin.Context) {
	var u user
	err := c.ShouldBind(&u) // 绑定结构体后，不管前端传来什么类型的数据，只要有匹配的字段，就会去自动匹配
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"err": err,
		})
	} else {
		fmt.Printf("%#v\n", u)
		c.JSON(http.StatusOK, gin.H{
			"message": "ok",
		})
	}
}

// 6.文件上传
// 处理multipart forms提交文件时默认的内存限制是32MB
// 可以通过r.MaxMultiPartMemory=8<<20 8M 修改
func uploadFile(c *gin.Context) {
	// 单个文件上传

	// 从请求中读取文件
	f, err := c.FormFile("f1") // 这个name要和前端的name一致
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	} else {
		// 将文件保存到本地
		dst := fmt.Sprintf("./%s", f.Filename)
		err := c.SaveUploadedFile(f, dst)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
			})
		}
	}

	// 多个文件上传
	//form,_ := c.MultipartForm()
	//files := form.File["file"]  // key值与前端对应
	//for index,file := range files{
	//	dst := fmt.Sprintf("./%s%d",file.Filename,index)
	//	c.SaveUploadedFile(file,dst)
	//}
	//c.JSON(http.StatusOK, gin.H{
	//	"status": "ok",
	//})
}

func upload(c *gin.Context) {
	c.HTML(http.StatusOK, "upload.html", nil)
}

// 7.请求重定向
func redirect(c *gin.Context) {
	c.Redirect(http.StatusMovedPermanently, "https://www.baidu.com")
}

func main() {
	r := gin.Default()

	r.LoadHTMLFiles("./login.html", "./home.html", "./upload.html")

	// 1.gin返回json格式数据
	r.GET("/json", backJson)
	// 2.gin获取浏览器请求携带的query string参数
	r.GET("/web", query)
	// 3.gin获取form表单参数
	r.GET("/login", GetLogin)
	r.POST("/login", PostLogin)
	//4.获取URI参数
	r.GET("/users/:username/:age", uri) // 使用:进行正则匹配
	// 5.gin参数绑定
	r.GET("/bind", bind)
	// 6.文件上传
	r.MaxMultipartMemory = 8 << 20 // 设置文件的最大内存
	r.GET("/upload", upload)
	r.POST("/upload", uploadFile)
	// 7.请求重定向
	r.GET("/redirect", redirect)

	err := r.Run(":9090")
	if err != nil {
		fmt.Printf("listen failed,err:%v\n", err)
		return
	}

}
