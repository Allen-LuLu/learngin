package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// 中间件：即在请求处理过程中，开发者可以加入自己的钩子(Hook)函数，进行一些公共的业务逻辑处理，比如登录认证，权限校验
// 数据分页，记录日志，耗时统计等，中间件通常与路由组联合使用
// 中间件是一个HandelFunc类型

func m1(c *gin.Context) {
	start := time.Now()
	fmt.Println("m1 in...")
	c.Next() // 阻塞执行下一个中间件函数，执行完后再执行后续的操作，如果没有这句话，则先执行完m1再执行m2
	end := time.Since(start)
	fmt.Println(end.String())
	fmt.Println("m1 out...")
}

func m2(c *gin.Context) {
	fmt.Println("m2 in...")
	//c.Abort() // 使用Abort()函数会阻止调用后续的中间件函数，执行完m2就会返回上一级
	//return    // 如果在此处加return，后面的都不执行了
	c.Next()
	fmt.Println("m2 out...")
}

func handle(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status": "success",
	})
}

// 实际场景中，中间件往往是通过闭包来实现的
func authMidWare(username string) gin.HandlerFunc {
	// 在这里可以做一些连接数据库的操作
	ok := false
	return func(c *gin.Context) {
		if !ok {
			c.Abort() // 如果不成功则阻止后面的操作
			c.JSON(http.StatusOK, gin.H{
				"err": "failed",
			})

		} else {
			c.Set("name", username) //可以通过set在上下文中获取值
			c.Next()
		}
	}
}

func login(c *gin.Context) {
	name, ok := c.Get("name")
	if !ok {
		name = "sb"
	}
	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"name":   name,
	})
}

func main() {
	r := gin.Default()

	rootGroup := r.Group("/", m1, m2)
	{
		rootGroup.GET("/index", handle)
		rootGroup.GET("/login", authMidWare("河清"), login)
	}

	err := r.Run(":9090")
	if err != nil {
		fmt.Printf("listen failed,err:%v\n", err)
		return
	}

}
